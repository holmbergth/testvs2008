﻿namespace ThomasWSTest
{
    partial class frmStart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grbText = new System.Windows.Forms.GroupBox();
            this.txtWSTime = new System.Windows.Forms.TextBox();
            this.btnMyButton = new System.Windows.Forms.Button();
            this.grbText.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbText
            // 
            this.grbText.Controls.Add(this.btnMyButton);
            this.grbText.Controls.Add(this.txtWSTime);
            this.grbText.Location = new System.Drawing.Point(47, 43);
            this.grbText.Name = "grbText";
            this.grbText.Size = new System.Drawing.Size(303, 285);
            this.grbText.TabIndex = 2;
            this.grbText.TabStop = false;
            // 
            // txtWSTime
            // 
            this.txtWSTime.Location = new System.Drawing.Point(9, 132);
            this.txtWSTime.Name = "txtWSTime";
            this.txtWSTime.Size = new System.Drawing.Size(285, 20);
            this.txtWSTime.TabIndex = 2;
            this.txtWSTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnMyButton
            // 
            this.btnMyButton.Location = new System.Drawing.Point(67, 158);
            this.btnMyButton.Name = "btnMyButton";
            this.btnMyButton.Size = new System.Drawing.Size(177, 23);
            this.btnMyButton.TabIndex = 4;
            this.btnMyButton.Text = "Tryck här";
            this.btnMyButton.UseVisualStyleBackColor = true;
            this.btnMyButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmStart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 388);
            this.Controls.Add(this.grbText);
            this.Name = "frmStart";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ThomasWsTest";
            this.grbText.ResumeLayout(false);
            this.grbText.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbText;
        private System.Windows.Forms.TextBox txtWSTime;
        private System.Windows.Forms.Button btnMyButton;

    }
}

